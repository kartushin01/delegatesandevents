﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DelegatesAndEvents
{
    public class FileSearcher
    {
        public FileSearcher(string path)
        {
            PathName = path;
        }
        public string PathName { get; set; }

        public event EventHandler<FileArgs> FileFound;
        protected virtual void OnFileFound(FileArgs e)
        {
            FileFound?.Invoke(this, e);
        }
        
        public void StartSerch(string triggerOnCancel = null)
        {
            foreach (var fileName in Directory.GetFiles(PathName).Select(Path.GetFileName))
            {
                FileFound?.Invoke(this, new FileArgs(fileName));
                
                if (triggerOnCancel == fileName)
                {
                    FileFound = null;
                }
            }

        }

   


    }
}
