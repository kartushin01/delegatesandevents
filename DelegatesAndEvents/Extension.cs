﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DelegatesAndEvents
{
    public static class Extension
    {
        public static T GetMax<T>(this List<T> e, Func<T, float> getParametr) where T : class
        {
            T max = e[0];

            foreach (var item in e)
            {
                if (getParametr(item) > getParametr(max))
                {
                    max = item;
                }
            }
            
            return max;

        }

    }
}
