﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace DelegatesAndEvents
{
    class Program
    {

        
        static void Main(string[] args)
        {
            //1. GetMax

            List<TestType> typesCollection = new List<TestType>();
           
            typesCollection.Add(new TestType(1));
            typesCollection.Add(new TestType(5));
            typesCollection.Add(new TestType(6));
            typesCollection.Add(new TestType(88));
            typesCollection.Add(new TestType(67));

            TestType resMax = typesCollection.GetMax(x => x.Value);

            Console.WriteLine($"Max : {resMax.Value}");

            //Search file

            string workingDirectory = Environment.CurrentDirectory;
            string slnDirectory = Directory.GetParent(workingDirectory).Parent.Parent.FullName;
            string path = Path.Combine(slnDirectory, "Files");

            string triggerOnCancel = "file2.txt";
            FileSearcher searcher = new FileSearcher(path);
            searcher.FileFound += Searcher_FileFound;
            searcher.StartSerch(triggerOnCancel);

        }

        private static void Searcher_FileFound(object sender, FileArgs e)
        {
            Console.WriteLine($"Найден файл : {e.Name}");
        }
    }
}
