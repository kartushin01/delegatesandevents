﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesAndEvents
{
    public class TestType
    {
        public TestType(float val)
        {
            Value = val;
        }
        public float Value { get; set; }
    }
}
