﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DelegatesAndEvents
{
    public class FileArgs : EventArgs
    {
        public string Name { get; set; }
        public bool CancelSearch { get; set; }
        public FileArgs(string fileName)
        {
            Name = fileName;
        }
    }
}
